namespace: "mpd-tools"
env: horsprod
global:
  seleniumGrid:
    # Image tag for all selenium components
    imageTag: latest
    # Url image for all selenium components
    urlImage: "dockerhub-docker-remote.artifact.cloud.socrate.vsct.fr/"
    # Image tag for browser's nodes
    nodesImageTag: latest
    # Pull secret for all components, can be overridden individually
    imagePullSecret: ""

# Deploy Router, Distributor, EventBus, SessionMap and Nodes separately
isolateComponents: true

deploymentLabels:
  track: stable
  # Active une règle réseau permettant de laisser entrer le trafic externe au cluster:
  type: front

# Configure the ingress resource to access the Grid installation.
ingress:
  # Enable or disable ingress resource
  enabled: true
  # Name of ingress class to select which controller will implement ingress resource
  className: ""

  # Default host for the ingress resource
  hostname: selenium-grid.local
  # TLS backend configuration for ingress resource
  tls: []
  # Domain name
  domainName: ritmx-dev.aws.vsct.fr
  # Custom annotations for ingress resource
  annotations:
    # mutualisation des ALB du namespace @see https://it4container.gitlab.cloud.socrate.vsct.fr/doc_client/aws/app-exposition/#mutualisation-dun-alb-pour-gerer-plusieurs-ingress
    # IMPORTANT: lire l'ADR "012-ADR-Gestion-des-alb-pour-nos-clusters-kubernetes.md" présent dans le repo mpd-v2
    alb.ingress.kubernetes.io/group.name: mpd # ALB group.name créé par défaut en même temps que le namespace dans k8s, impossible à supprimer
    alb.ingress.kubernetes.io/target-type: ip # pour que le calcul de la limite de target pour l'ALB ne prenne en compte que les noeuds du cluster k8s sur lesquels sont les déployés PODS
    alb.ingress.kubernetes.io/listen-ports: '[{"HTTP": 80},{"HTTPS":443}]'
    alb.ingress.kubernetes.io/ssl-redirect: '443'
    alb.ingress.kubernetes.io/certificate-arn: arn:aws:acm:eu-west-3:750343639269:certificate/8bdfc5bb-54c3-4767-adc7-95b943381780
    alb.ingress.kubernetes.io/security-groups: k8s-albSecurityGroup


proxy:
  name: ""

# ConfigMap that contains SE_EVENT_BUS_HOST, SE_EVENT_BUS_PUBLISH_PORT and SE_EVENT_BUS_SUBSCRIBE_PORT variables
busConfigMap:
  # Name of the configmap
  name: selenium-event-bus-config
  # Custom annotations for configmap
  annotations: {}


application:
  name: seleniumGrid proxy
# Configuration for isolated components (applied only if `isolateComponents: true`)
components:

  # Configuration for router component
  router:
    # Router image name
    imageName: selenium/router
    # Router image tag (this overwrites global.seleniumGrid.imageTag parameter)
    # imageTag: 4.8.1-20230306

    # Image pull policy (see https://kubernetes.io/docs/concepts/containers/images/#updating-images)
    imagePullPolicy: IfNotPresent
    # Image pull secret (see https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)
    imagePullSecret: ""

    # Custom annotations for router pods
    annotations: {}
    # Router port
    port: 4444
    # Liveness probe settings
    livenessProbe:
      enabled: true
      path: /readyz
      initialDelaySeconds: 10
      failureThreshold: 10
      timeoutSeconds: 10
      periodSeconds: 10
      successThreshold: 1
    # Readiness probe settings
    readinessProbe:
      enabled: true
      path: /readyz
      initialDelaySeconds: 12
      failureThreshold: 10
      timeoutSeconds: 10
      periodSeconds: 10
      successThreshold: 1
    # Resources for router container
    resources:
      limits:
        cpu: ".5"
        memory: "1000Mi"
    # Kubernetes service type (see https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types)
    serviceType: NodePort
    # Custom annotations for router service
    serviceAnnotations:
      service.beta.kubernetes.io/aws-load-balancer-type: external
      service.beta.kubernetes.io/aws-load-balancer-nlb-target-type: ip
      external-dns.alpha.kubernetes.io/hostname: selenium-grid-mpd-horsprod.ritmx-dev.aws.vsct.fr
      service.beta.kubernetes.io/aws-load-balancer-additional-resource-tags: cost:project=mpd, cost:application=mpd, cost:environment=dev
    # Tolerations for router pods
    tolerations: []
    # Node selector for router pods
    nodeSelector: {}
    # Priority class name for router pods
    priorityClassName: ""

  # Configuration for distributor component
  distributor:
    # Distributor image name
    imageName: selenium/distributor
    # Distributor image tag (this overwrites global.seleniumGrid.imageTag parameter)
    # imageTag: 4.8.1-20230306

    # Image pull policy (see https://kubernetes.io/docs/concepts/containers/images/#updating-images)
    imagePullPolicy: IfNotPresent
    # Image pull secret (see https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)
    imagePullSecret: ""

    # Custom annotations for Distributor pods
    annotations: {}
    # Distributor port
    port: 5553
    # Resources for Distributor container
    resources:
      limits:
        cpu: ".5"
        memory: "1000Mi"
    # Kubernetes service type (see https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types)
    serviceType: ClusterIP
    # Custom annotations for Distributor service
    serviceAnnotations: {}
    # Tolerations for Distributor pods
    tolerations: []
    # Node selector for Distributor pods
    nodeSelector: {}
    # Priority class name for Distributor pods
    priorityClassName: ""

  # Configuration for Event Bus component
  eventBus:
    # Event Bus image name
    imageName: selenium/event-bus
    # Event Bus image tag (this overwrites global.seleniumGrid.imageTag parameter)
    # imageTag: 4.8.1-20230306

    # Image pull policy (see https://kubernetes.io/docs/concepts/containers/images/#updating-images)
    imagePullPolicy: IfNotPresent
    # Image pull secret (see https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)
    imagePullSecret: ""

    # Custom annotations for Event Bus pods
    annotations: {}
    # Event Bus port
    port: 5557
    # Port where events are published
    publishPort: 4442
    # Port where to subscribe for events
    subscribePort: 4443
    # Resources for event-bus container
    resources:
      limits:
        cpu: ".5"
        memory: "1000Mi"
    # Kubernetes service type (see https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types)
    serviceType: ClusterIP
    # Custom annotations for Event Bus service
    serviceAnnotations: {}
    # Tolerations for Event Bus pods
    tolerations: []
    # Node selector for Event Bus pods
    nodeSelector: {}
    # Priority class name for Event Bus pods
    priorityClassName: ""

  # Configuration for Session Map component
  sessionMap:
    # Session Map image name
    imageName: selenium/sessions
    # Session Map image tag (this overwrites global.seleniumGrid.imageTag parameter)
    # imageTag: 4.8.1-20230306

    # Image pull policy (see https://kubernetes.io/docs/concepts/containers/images/#updating-images)
    imagePullPolicy: IfNotPresent
    # Image pull secret (see https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)
    imagePullSecret: ""

    # Custom annotations for Session Map pods
    annotations: {}
    port: 5556
    # Resources for Session Map container
    resources:
      limits:
        cpu: ".5"
        memory: "1000Mi"
    # Kubernetes service type (see https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types)
    serviceType: ClusterIP
    # Custom annotations for Session Map service
    serviceAnnotations: {}
    # Tolerations for Session Map pods
    tolerations: []
    # Node selector for Session Map pods
    nodeSelector: {}
    # Priority class name for Session Map pods
    priorityClassName: ""

  # Configuration for Session Queue component
  sessionQueue:
    # Session Queue image name
    imageName: selenium/session-queue
    # Session Queue image tag (this overwrites global.seleniumGrid.imageTag parameter)
    # imageTag: 4.8.1-20230306

    # Image pull policy (see https://kubernetes.io/docs/concepts/containers/images/#updating-images)
    imagePullPolicy: IfNotPresent
    # Image pull secret (see https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)
    imagePullSecret: ""

    # Custom annotations for Session Queue pods
    annotations: {}
    port: 5559
    # Resources for Session Queue container
    resources:
      limits:
        cpu: ".5"
        memory: "1000Mi"
    # Kubernetes service type (see https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types)
    serviceType: ClusterIP
    # Custom annotations for Session Queue service
    serviceAnnotations: {}
    # Tolerations for Session Queue pods
    tolerations: []
    # Node selector for Session Queue pods
    nodeSelector: {}
    # Priority class name for Session Queue pods
    priorityClassName: ""
  proxy:
    port: 3128
    serviceAnnotations: {}
    # Kubernetes service type (see https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types)
    serviceType: ClusterIP

  # Custom environment variables for all components
  extraEnvironmentVariables:
  # - name: SE_JAVA_OPTS
  #   value: "-Xmx512m"
  # - name:
  #   valueFrom:
  #     secretKeyRef:
  #       name: secret-name
  #       key: secret-key

  # Custom environment variables by sourcing entire configMap, Secret, etc. for all components
  extraEnvFrom:
  # - configMapRef:
  #   name: proxy-settings
  # - secretRef:
  #   name: mysecret

# Configuration for selenium hub deployment (applied only if `isolateComponents: false`)
hub:
  # Selenium Hub image name
  imageName: selenium/hub
  # Selenium Hub image tag (this overwrites global.seleniumGrid.imageTag parameter)
  # imageTag: 4.8.1-20230306
  # Image pull policy (see https://kubernetes.io/docs/concepts/containers/images/#updating-images)
  imagePullPolicy: IfNotPresent
  # Image pull secret (see https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)
  imagePullSecret: ""

  # Custom annotations for Selenium Hub pods
  annotations: {}
  # Custom labels for Selenium Hub pods
  labels: {}
  # Port where events are published
  publishPort: 4442
  # Port where to subscribe for events
  subscribePort: 4443
  # Selenium Hub port
  port: 4444
  # Liveness probe settings
  livenessProbe:
    enabled: true
    path: /readyz
    initialDelaySeconds: 10
    failureThreshold: 10
    timeoutSeconds: 10
    periodSeconds: 10
    successThreshold: 1
  # Readiness probe settings
  readinessProbe:
    enabled: true
    path: /readyz
    initialDelaySeconds: 12
    failureThreshold: 10
    timeoutSeconds: 10
    periodSeconds: 10
    successThreshold: 1
  # Custom environment variables for selenium-hub
  extraEnvironmentVariables:
  # - name: SE_JAVA_OPTS
  #   value: "-Xmx512m"
  # - name: SECRET_VARIABLE
  #   valueFrom:
  #     secretKeyRef:
  #       name: secret-name
  #       key: secret-key
  # Custom environment variables by sourcing entire configMap, Secret, etc. for selenium-hub
  extraEnvFrom:
  # - configMapRef:
  #   name: proxy-settings
  # - secretRef:
  #   name: mysecret
  # Resources for selenium-hub container
  resources:
    limits:
      cpu: "500m"
      memory: "512Mi"
  # Kubernetes service type (see https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types)
  serviceType: ClusterIP
  # Set specific loadBalancerIP when serviceType is LoadBalancer (see https://kubernetes.io/docs/concepts/services-networking/service/#loadbalancer)
  loadBalancerIP: ""
  # Custom annotations for Selenium Hub service
  serviceAnnotations: {}
  # Tolerations for selenium-hub pods
  tolerations: []
  # Node selector for selenium-hub pods
  nodeSelector: {}
  # Priority class name for selenium-hub pods
  priorityClassName: ""

# Configuration for firefox nodes
firefoxNode:
  # Enable firefox nodes
  enabled: true

  # Enable creation of Deployment
  # true (default) - if you want long living pods
  # false - for provisioning your own custom type such as Jobs
  deploymentEnabled: true

  # Number of firefox nodes
  replicas: 1
  # Image of firefox nodes
  imageName: selenium/node-firefox
  # Image of firefox nodes (this overwrites global.seleniumGrid.nodesImageTag)
  # imageTag: 4.8.1-20230306
  # Image pull policy (see https://kubernetes.io/docs/concepts/containers/images/#updating-images)
  imagePullPolicy: IfNotPresent
  # Image pull secret (see https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)
  imagePullSecret: ""

  # Port to node
  nodePort: 5555
  # Selenium port (spec.ports[0].targetPort in kubernetes service)
  seleniumPort: 5900
  # Annotations for firefox-node pods
  annotations: {}
  # Labels for firefox-node pods
  labels: {}
  # Tolerations for firefox-node pods
  tolerations: []
  # Node selector for firefox-node pods
  nodeSelector: {}
  # Resources for firefox-node container
  resources:
    requests:
      memory: "1Gi"
      cpu: "1"
    limits:
      memory: "1Gi"
      cpu: "1"
  # Custom host aliases for firefox nodes
  hostAliases:
  # - ip: "198.51.100.0"
  #   hostnames:
  #     - "example.com"
  #     - "example.net"
  # - ip: "203.0.113.0"
  #   hostnames:
  #     - "example.org"
  # Custom environment variables for firefox nodes
  extraEnvironmentVariables:
  # - name: SE_JAVA_OPTS
  #   value: "-Xmx512m"
  # - name:
  #   valueFrom:
  #     secretKeyRef:
  #       name: secret-name
  #       key: secret-key
  # Custom environment variables by sourcing entire configMap, Secret, etc. for firefox nodes
  extraEnvFrom:
  # - configMapRef:
  #   name: proxy-settings
  # - secretRef:
  #   name: mysecret
  # Service configuration
  service:
    # Create a service for node
    enabled: true
    # Service type
    type: ClusterIP
    # Custom annotations for service
    annotations: {}
  # Size limit for DSH volume mounted in container (if not set, default is "1Gi")
  dshmVolumeSizeLimit: 1Gi
  # Priority class name for firefox-node pods
  priorityClassName: ""

  # Wait for pod startup
  startupProbe: {}
    # httpGet:
    #   path: /status
    #   port: 5555
  # failureThreshold: 120
  # periodSeconds: 5
  # Time to wait for pod termination
  terminationGracePeriodSeconds: 30
  # Allow pod correctly shutdown
  lifecycle: {}
    # preStop:
    #   exec:
    #     command:
    #       - bash
    #       - -c
    #       - |
  #         curl -X POST 127.0.0.1:5555/se/grid/node/drain --header 'X-REGISTRATION-SECRET;' && \
  #         while curl 127.0.0.1:5555/status; do sleep 1; done

  extraVolumeMounts: []
  # - name: my-extra-volume
  #   mountPath: /home/seluser/Downloads

  extraVolumes: []
  # - name: my-extra-volume
  #   emptyDir: {}
  # - name: my-extra-volume-from-pvc
  #   persistentVolumeClaim:
  #     claimName: my-pv-claim

# Custom labels for k8s resources
customLabels: {}
