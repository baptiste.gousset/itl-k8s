#!/bin/bash
SPACE=40
NB_TESTS=0
NB_TESTS_OK=0

#############################################
## Utils
#############################################
function print_red() {
  printf "\e[1m\e[31m" ; printf "$1" ; printf "\e[0m\n";
}
function print_green() {
  printf "\e[1m\e[32m" ; printf "$1" ; printf "\e[0m\n";
}

function print_result {
  printf "==================================================================\n"
  if [ "$NB_TESTS_OK" -ne "$NB_TESTS" ]; then
     print_red "There are tests failures"
     exit 1;
  else
    print_green "All tests are valid"
  fi
  printf "%s / %s\n" "$NB_TESTS_OK" "$NB_TESTS"
  printf "==================================================================\n"
}

function assert_is_not_empty {
  VAR=$1
  TEST=$2

  NB_TESTS=$((NB_TESTS+1))
  RESULT=$(print_red "KO")
  if [ -n "$POD" ]; then
    RESULT=$(print_green "OK")
    NB_TESTS_OK=$((NB_TESTS_OK+1))
  fi
  printf "%-${SPACE}s%s\n" "$TEST" "$RESULT"
}

function assert_is_equal {
  LEFT=$1
  RIGHT=$2
  TEST=$3

  NB_TESTS=$((NB_TESTS+1))
  RESULT=$(print_red "KO")
  if [ "$LEFT" = "$RIGHT" ]; then
    RESULT=$(print_green "OK")
    NB_TESTS_OK=$((NB_TESTS_OK+1))
  fi
  printf "%-${SPACE}s%s\n" "$TEST" "$RESULT"
}

#############################################
## Kube testing
#############################################
function assert_pod_is_up {
  local NAMESPACE=$1
  local POD_NAME=$2
  local TEST=${3-"Test Pod $POD_NAME is up"}

  POD=$(kubectl get -n "$NAMESPACE" pods --field-selector status.phase=Running -l "app=$POD_NAME" --no-headers=true 2> /dev/null)

  assert_is_not_empty "$POD" "$TEST"
}

function assert_has_tcp_connection_to {
  local NAMESPACE=$1
  local SELECTOR=$2
  local HOST=$3
  local PORT=$4
  local TEST=${5-"Test $SELECTOR has TCP connection to $HOST:$PORT"}

  kubectl exec -n "$NAMESPACE" "$SELECTOR" --request-timeout 5s -- bash -c "echo '' > /dev/tcp/$HOST/$PORT" 2> /dev/null
  assert_is_equal "$?" "0" "$TEST"
}

function assert_has_http_connection_to {
   local NAMESPACE=$1
   local SELECTOR=$2
   local URL=$3
   local TEST=${4-"Test $SELECTOR has HTTP connection to $URL"}

   local TIMEOUT="--max-time 5 --connect-timeout 5"
   kubectl exec -n "$NAMESPACE" "$SELECTOR" --request-timeout 5s -- bash -c "curl -sI '$URL' $TIMEOUT > /dev/null" 2> /dev/null
   assert_is_equal "$?" "0" "$TEST"
}

function assert_http_access {
   local URL=$1
   local TEST=${2-"Test $URL is available"}

   local TIMEOUT="--max-time 5 --connect-timeout 5"
   curl -sI "$URL" -o /dev/null $TIMEOUT
   assert_is_equal "$?" "0" "$TEST"
}
#############################################
## Tests
#############################################

DOMAIN=".mpd-tools.svc.cluster.local"

# Bus
BUS="selenium-event-bus-horsprod"
assert_pod_is_up "mpd-tools" "$BUS" "Bus is UP"

# Session Map
MAP="selenium-session-map-horsprod"
assert_pod_is_up "mpd-tools" "$MAP" "Session Map is UP"
assert_has_tcp_connection_to "mpd-tools" "service/$MAP-svc" "$BUS-svc" "4442" "Session Map -> Bus"

# Session Queue
QUEUE="selenium-session-queue-horsprod"
assert_pod_is_up "mpd-tools" "$QUEUE" "Session Queue is UP"
assert_has_tcp_connection_to "mpd-tools" "service/$QUEUE-svc" "$BUS-svc" "4442" "Session Queue -> Bus"

# Distributor
DISTRIB="selenium-distributor-horsprod"
assert_pod_is_up "mpd-tools" "$DISTRIB" "Distributor is UP"
assert_has_tcp_connection_to "mpd-tools" "service/$DISTRIB-svc" "$BUS-svc" "4442" "Distributor -> Bus"

# Router
ROUTER="selenium-router-horsprod"
assert_pod_is_up "mpd-tools" "$ROUTER" "Router is UP"
assert_http_access "https://selenium-grid-mpd-horsprod.ritmx-dev.aws.vsct.fr/status" "Ingress is UP and Ready"

# Node
NODE="selenium-firefox-node-horsprod"
assert_pod_is_up "mpd-tools" "$NODE" "Firefox Node is UP"
assert_has_tcp_connection_to "mpd-tools" "service/$NODE-svc" "$BUS-svc" "4442" "Firefox Node -> Bus"
assert_has_http_connection_to "mpd-tools" "service/$ROUTER-svc" "http://$NODE-svc:5555/status" "HTTP Access Router -> Firefox Node"
assert_has_http_connection_to "mpd-tools" "service/$NODE-svc" "http://$ROUTER-svc$DOMAIN:4444/status" "HTTP Access Firefox Node -> Router"
assert_has_http_connection_to "mpd-tools" "service/$NODE-svc" "https://www.google.com" "HTTP Access Firefox Node -> Google"

print_result