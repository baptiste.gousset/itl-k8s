Selenium Grid Server deployed successfully.

{{- $svcName := ternary (include "seleniumGrid.router.fullname" . ) (include "seleniumGrid.hub.fullname" . ) .Values.isolateComponents }}
{{- $appName := ternary "selenium-router" "selenium-hub" .Values.isolateComponents }}
{{- $serviceType := ternary .Values.components.router.serviceType .Values.hub.serviceType .Values.isolateComponents }}
{{- $port := ternary .Values.components.router.port .Values.hub.port .Values.isolateComponents }}
{{- $localUrl := ternary "http://localhost:PORT" "http://localhost:PORT/wd/hub" .Values.isolateComponents }}

1. Ingress is enabled, but hostname doesn't set. All inbound HTTP traffic will be routed to the Grid by matching any host.
    Please keep in mind that it is rarely necessary, and in most cases, you shall provide `ingress.hostname` in values.yaml.
    To access Selenium from outside of Kubernetes:
        - open IP of the any node with Ingress, or
        - any hostname pointing to the node with Ingress

2. Within Kubernetes cluster, you can use following Service endpoint:
        https://{{ .Release.Name }}-{{ .Values.env }}.{{ .Values.ingress.domainName }}
